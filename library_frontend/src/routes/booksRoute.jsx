import { Route } from 'react-router-dom';
import React from 'react';
import BooksPage from '../pages/BooksPage/BooksPage.jsx';
import Private from './Private.jsx';

const booksRoute = () => (
  <Route exact path="/books" element={<Private />}>
    <Route exact path="/books" element={<BooksPage />} />
  </Route>
);
export default booksRoute;
