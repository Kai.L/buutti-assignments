import React, { useContext } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { AuthContext } from '../Contexts/AuthContext.jsx';

const Private = function PrivateComponent() {
  const { authState } = useContext(AuthContext);
  return authState.isLoggedIn ? <Outlet /> : <Navigate to="/" />;
};

export default Private;
