import React, { useContext } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { AuthContext } from '../Contexts/AuthContext.jsx';

const Admin = function AdminComponent() {
  const { authState } = useContext(AuthContext);

  return authState.isLoggedIn && authState.isAdmin ? <Outlet /> : <Navigate to="/" />;
};

export default Admin;
