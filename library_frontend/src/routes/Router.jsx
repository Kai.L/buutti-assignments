import React from 'react';
import { BrowserRouter, Routes } from 'react-router-dom';
import NavBar from '../Components/NavBar/NavBar.jsx';
import booksRoute from './booksRoute.jsx';
import accountRoute from './accountRoute.jsx';
import adminRoute from './adminRoute.jsx';
import frontPageRoute from './frontPageRoute.jsx';
import Notification from '../Components/Notification/Notification.jsx';
import { AuthProvider } from '../Contexts/AuthContext.jsx';
import { LibraryProvider } from '../Contexts/LibraryContext.jsx';
import { NotificationProvider } from '../Contexts/NotificationContext.jsx';

const Router = function RouterComponent() {
  return (
    <BrowserRouter>
      <AuthProvider>
        <LibraryProvider>
          <NotificationProvider>
            <NavBar />
            <Notification />
            <Routes>
              {frontPageRoute()}
              {booksRoute()}
              {accountRoute()}
              {adminRoute()}
            </Routes>
          </NotificationProvider>
        </LibraryProvider>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Router;
