import { Route } from 'react-router-dom';
import React from 'react';
import AdminPage from '../pages/AdminPage/AdminPage.jsx';
import Admin from './Admin.jsx';

const adminRoute = () => (
  <Route exact path="/admin" element={<Admin />}>
    <Route exact path="/admin" element={<AdminPage />} />
  </Route>
);
export default adminRoute;
