import { Route } from 'react-router-dom';
import React from 'react';
import FrontPage from '../pages/FrontPage/FrontPage.jsx';

const frontPageRoute = () => <Route exact path="/" element={<FrontPage />} />;

export default frontPageRoute;
