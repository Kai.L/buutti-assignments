import { Route } from 'react-router-dom';
import React from 'react';
import AccountPage from '../pages/AccountPage/AccountPage.jsx';
import Private from './Private.jsx';

const accountRoute = () => (
  <Route exact path="/account" element={<Private />}>
    <Route exact path="/account" element={<AccountPage />} />
  </Route>
);
export default accountRoute;
