import React, {
  createContext, useContext, useState, useMemo, useCallback,
} from 'react';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import PropTypes from 'prop-types';
import { NotificationContext } from './NotificationContext.jsx';

export const AuthContext = createContext({
  authState: { isLoggedIn: false, isAdmin: false },
  login: () => null,
  register: () => null,
  logout: () => null,
  decodedToken: {},
});

export const AuthProvider = function AuthProviderComponent({ children }) {
  const { setNotificationState } = useContext(NotificationContext);
  const token = localStorage.getItem('token');
  let decodedToken = useMemo(() => ({}), []);

  if (token) {
    try {
      decodedToken = jwtDecode(token);
    } catch (e) {
      setNotificationState('Token is malformed.');
    }
  }

  const [authState, setAuthState] = useState({
    isLoggedIn: !!token, isAdmin: !!decodedToken.admin,
  });

  const login = useCallback((username, password) => {
    axios.post('http://localhost:3001/account/login', {
      name: username, password,
    })
      .then((res) => {
        const tokenFromLoginResponse = res.data.fetchedValue.token;
        if (tokenFromLoginResponse) {
          localStorage.setItem('token', tokenFromLoginResponse);
          try {
            const tokenFromLoginResponseDecoded = jwtDecode(tokenFromLoginResponse);
            setAuthState({
              isLoggedIn: !!tokenFromLoginResponse,
              isAdmin: tokenFromLoginResponseDecoded.admin,
            });
          } catch (e) {
            setAuthState({ isLoggedIn: false, isAdmin: false });
          }
        }
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  }, [setNotificationState]);

  const register = useCallback((username, password) => {
    axios.post('http://localhost:3001/account', {
      name: username, password,
    })
      .then((res) => {
        const tokenFromRegisterResponse = res.data.createdValue.token;
        if (tokenFromRegisterResponse) {
          localStorage.setItem('token', tokenFromRegisterResponse);
          try {
            const tokenFromRegisterResponseDecoded = jwtDecode(tokenFromRegisterResponse);
            setAuthState({
              isLoggedIn: !!tokenFromRegisterResponse,
              isAdmin: tokenFromRegisterResponseDecoded.admin,
            });
          } catch (e) {
            setAuthState({ isLoggedIn: false, isAdmin: false });
          }
        }
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  }, [setNotificationState]);
  const logout = useCallback(() => {
    localStorage.removeItem('token');
    setAuthState({ isLoggedIn: false, isAdmin: false });
  }, []);

  const valuesToProvide = useMemo(
    () => ({
      authState, login, register, logout, decodedToken,
    }),
    [authState, login, register, logout, decodedToken],
  );

  return (
    <AuthContext.Provider value={valuesToProvide}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
