import React, { createContext, useState, useMemo } from 'react';
import PropTypes from 'prop-types';

export const NotificationContext = createContext({
  notificationState: '',
  setNotificationState: () => null,
});

export const NotificationProvider = function NotificationProviderComponent({ children }) {
  const [notificationState, setNotificationState] = useState('');

  setTimeout(() => {
    setNotificationState('');
  }, [8000]);

  const valuesToProvide = useMemo(
    () => ({
      notificationState, setNotificationState,
    }),
    [notificationState, setNotificationState],
  );

  return (
    <NotificationContext.Provider value={valuesToProvide}>
      {children}
    </NotificationContext.Provider>
  );
};

NotificationProvider.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
};
