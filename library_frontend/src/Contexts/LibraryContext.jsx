import React, {
  createContext, useContext, useEffect, useState, useMemo,
} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { AuthContext } from './AuthContext.jsx';
import { NotificationContext } from './NotificationContext.jsx';

export const LibraryContext = createContext({
  booksState: [],
  setBooksState: () => null,
});

export const LibraryProvider = function LibraryProviderComponent({ children }) {
  const { setNotificationState } = useContext(NotificationContext);
  const { authState } = useContext(AuthContext);
  const [booksState, setBooksState] = useState([]);

  useEffect(() => {
    const getBooks = () => {
      axios.get('http://localhost:3001/books', { headers: { Authorization: localStorage.getItem('token') } }).then((res) => {
        setBooksState([res.data.fetchedValue].flat());
      })
        .catch((e) => {
          setNotificationState(e.response);
        });
    };

    if (authState.isLoggedIn) {
      getBooks();
    }
  }, [authState, setNotificationState]);

  const valuesToProvide = useMemo(
    () => ({
      booksState,
      setBooksState,
    }),
    [booksState],
  );

  return (
    <LibraryContext.Provider value={valuesToProvide}>
      {children}
    </LibraryContext.Provider>
  );
};

LibraryProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
