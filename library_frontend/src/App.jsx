import React from 'react';
import Router from './routes/Router.jsx';

const App = function AppComponent() {
  return (
    <div className="App">
      <Router />
    </div>
  );
};

export default App;
