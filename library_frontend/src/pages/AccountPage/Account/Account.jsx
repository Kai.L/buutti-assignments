import React, { useContext } from 'react';
import jwtDecode from 'jwt-decode';
import './Account.css';
import axios from 'axios';
import { AuthContext } from '../../../Contexts/AuthContext.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const Account = function AccountComponent() {
  const { logout } = useContext(AuthContext);
  const { setNotificationState } = useContext(NotificationContext);

  const deleteAccount = (logoutParameter, setNotificationStateParameter) => {
    const token = localStorage.getItem('token');
    let decodedToken = {};

    if (token) {
      try {
        decodedToken = jwtDecode(token);
      } catch (e) {
        setNotificationStateParameter('Token is malformed.');
      }
    }
    axios.delete('http://localhost:3001/account', {
      data: { id: decodedToken.sub },
      headers: { Authorization: localStorage.getItem('token') },
    })
      .then((response) => {
        setNotificationStateParameter(response.data.msg);
        logoutParameter();
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };

  return (
    <div className="Account">
      <div className="info_Account" />
      <button
        type="button"
        onClick={() => {
          deleteAccount(logout, setNotificationState);
        }}
        className="deleteBtn_Account"
      >
        Poista tili
      </button>
    </div>
  );
};

export default Account;
