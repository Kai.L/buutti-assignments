import React from 'react';
import Account from './Account/Account.jsx';
import './AccountPage.css';
import BorrowedList from './BorrowedList/BorrowedList.jsx';

const AccountPage = function AccountPageComponent() {
  return (
    <div className="AccountPage">
      <Account />
      <BorrowedList />
    </div>
  );
};

export default AccountPage;
