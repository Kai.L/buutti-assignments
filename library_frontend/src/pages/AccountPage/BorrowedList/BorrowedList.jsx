import React, { useContext } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import './BorrowedList.css';
import { AuthContext } from '../../../Contexts/AuthContext.jsx';
import Loading from '../../../Components/Loading/Loading.jsx';

const BorrowedList = function BorrowedListComponent() {
  const { booksState, setBooksState } = useContext(LibraryContext);
  const { setNotificationState } = useContext(NotificationContext);
  const { decodedToken } = useContext(AuthContext);

  const returnBook = (idParameter) => {
    axios.put(
      'http://localhost:3001/books/return',
      { bookId: idParameter },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((res) => {
        const updatedBooks = booksState.map((book) => {
          const bookCopy = { ...book };
          if (bookCopy.id === idParameter) {
            bookCopy.account = null;
            bookCopy.to_be_returned = null;
          }
          return bookCopy;
        });
        setBooksState(updatedBooks);
        setNotificationState(res.data.msg);
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  };

  const getBooks = (books) => books.map((book) => {
    const returnDate = new Date(book.to_be_returned);
    if (decodedToken.sub === book.account) {
      return (
        <div className="borrowedBook_BorrowedList" key={uuidv4()}>
          <p className="bookTitle_BorrowedList">{book.title}</p>
          <div className="bookInfoWrapper_BorrowedList">
            <p className="bookInfo_BorrowedList">{`Vuosi: ${book.year}`}</p>
            <p className="bookInfo_BorrowedList">{`Kategoria: ${book.gategory}`}</p>
            <p className="bookInfo_BorrowedList">{`Kirjasto: ${book.location}`}</p>
            <p
              className="bookInfo_BorrowedList"
            >
              {`Palautus viimeistään: ${returnDate.getDate()}.${returnDate.getMonth() + 1}.${returnDate.getFullYear()}`}
            </p>
          </div>
          <button
            type="button"
            onClick={() => { returnBook(book.id); }}
            className="returnButton_BorrowedList"
          >
            Palauta
          </button>
        </div>
      );
    }
    return null;
  });

  if (!booksState[0]) {
    return <Loading />;
  }

  return (
    <div className="BorrowedList">
      <div className="booksWrapper_BorrowedList">
        {getBooks(booksState)}
      </div>
    </div>
  );
};

export default BorrowedList;
