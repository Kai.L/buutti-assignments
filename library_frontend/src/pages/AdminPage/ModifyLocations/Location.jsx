import React, { useContext, useState } from 'react';
import axios from 'axios';
import './Location.css';
import PropTypes from 'prop-types';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const Location = function LocationComponent({
  id, name, locationsState, setLocationsState,
}) {
  const { setNotificationState } = useContext(NotificationContext);
  const [modifySelected, setModifySelected] = useState(false);
  const [locationName, setLocationName] = useState(name);

  const modifyLocation = (
    idParameter,
    nameParameter,
    locationsStateParameter,
    setLocationsStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.put(
      'http://localhost:3001/location',
      { id: idParameter, name: nameParameter },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const updatedLocations = locationsStateParameter.map((location) => {
          const locationCopy = location;
          if (locationCopy.id === id) {
            locationCopy.name = name;
          }
          return locationCopy;
        });
        setLocationsStateParameter(updatedLocations);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };

  const deleteLocation = (
    idParameter,
    locationsStateParameter,
    setLocationsStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.delete('http://localhost:3001/location', {
      headers: { Authorization: localStorage.getItem('token') },
      data: { id: idParameter },
    })
      .then((response) => {
        const updatedLocations = locationsStateParameter.filter((location) => location.id !== id);
        setLocationsStateParameter(updatedLocations);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };
  return (
    <div className="Location">
      {!modifySelected
        ? (
          <div>
            <p className="info_Location">{name}</p>
            <div className="buttonsWrapper_Location">
              <button
                type="button"
                className="blueButton_Location"
                onClick={() => {
                  setModifySelected(true);
                }}
              >
                Muokkaa
              </button>
              <button
                type="button"
                className="redButton_Location"
                onClick={() => {
                  deleteLocation(id, locationsState, setLocationsState, setNotificationState);
                }}
              >
                Poista
              </button>
            </div>
          </div>
        )

        : (
          <div>
            <input
              className="input_Location"
              placeholder="Nimi"
              value={locationName}
              onChange={(e) => { setLocationName(e.target.value); }}
            />
            <div className="buttonsWrapper_Location">
              <button
                type="button"
                className="blueButton_Location"
                onClick={() => {
                  modifyLocation(
                    id,
                    locationName,
                    locationsState,
                    setLocationsState,
                    setNotificationState,
                  );
                }}
              >
                Vahvista
              </button>
              <button
                type="button"
                className="redButton_Location"
                onClick={() => { setModifySelected(false); }}
              >
                Peruuta
              </button>
            </div>
          </div>
        )}
    </div>
  );
};

export default Location;

Location.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  locationsState: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
  setLocationsState: PropTypes.func.isRequired,
};
