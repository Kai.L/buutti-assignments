import React, { useContext, useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const CreateForm = function CreateFormComponent({ locationsState, setLocationsState }) {
  const { setNotificationState } = useContext(NotificationContext);
  const [visibility, setVisibility] = useState(false);
  const [location, setLocation] = useState('');

  const createNew = (
    nameParameter,
    locationsStateParameter,
    setLocationsStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.post(
      'http://localhost:3001/location',
      { name: nameParameter },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const udpatedLocations = [
          ...locationsStateParameter,
          {
            id: response.data.createdValue.id,
            name: nameParameter,
          },
        ];
        setLocationsStateParameter(udpatedLocations);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };
  return (
    <div className="CreateForm">
      {!visibility
        ? (
          <button
            type="button"
            onClick={() => {
              setVisibility(!visibility);
            }}
            className="formBtn_CreateForm"
          >
            Uusi
          </button>
        )
        : (
          <div>
            <button
              type="button"
              onClick={() => { setVisibility(!visibility); }}
              className="closeFormBtn_CreateForm"
            >
              Sulje
            </button>
            <input
              className="input_CreateForm"
              placeholder="Nimi"
              value={location}
              onChange={(e) => { setLocation(e.target.value); }}
            />
            <button
              type="button"
              onClick={() => {
                createNew(location, locationsState, setLocationsState, setNotificationState);
              }}
              className="formBtn_CreateForm"
            >
              Vahvista
            </button>
          </div>
        )}
    </div>
  );
};

export default CreateForm;

CreateForm.propTypes = {
  locationsState: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
  setLocationsState: PropTypes.func.isRequired,
};
