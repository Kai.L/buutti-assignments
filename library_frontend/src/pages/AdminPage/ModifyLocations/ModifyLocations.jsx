import React, { useContext, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import Location from './Location.jsx';
import SearchObjectsFieldComponent from '../../../Components/Search/SearchObjectsField.jsx';
import Loading from '../../../Components/Loading/Loading.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import CreateForm from './CreateForm.jsx';

const ModifyLocations = function ModifyLocationComponent() {
  const [locationsState, setLocationsState] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const { setNotificationState } = useContext(NotificationContext);

  useEffect(() => {
    const getLocations = () => {
      axios.get('http://localhost:3001/location')
        .then((res) => {
          setLocationsState([res.data.fetchedValue].flat());
        })
        .catch((e) => {
          setNotificationState(e.response.data.msg);
        });
    };
    getLocations();
  }, [setNotificationState]);

  const getLocations = () => searchResults.map((g) => (
    <Location
      key={uuidv4()}
      id={g.id}
      name={g.name}
      locationsState={locationsState}
      setLocationsState={setLocationsState}
    />
  ));

  if (!locationsState[0]) {
    return <Loading />;
  }
  return (
    <div className="ModifyLocations">
      <CreateForm locationsState={locationsState} setLocationsState={setLocationsState} />
      <SearchObjectsFieldComponent
        searchKey="name"
        values={locationsState}
        setResult={setSearchResults}
      />
      {getLocations()}
    </div>
  );
};

export default ModifyLocations;
