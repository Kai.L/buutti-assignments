import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import './ModifyAdmins.css';
import { v4 as uuidv4 } from 'uuid';
import SearchObjectsField from '../../../Components/Search/SearchObjectsField.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import Loading from '../../../Components/Loading/Loading.jsx';

const ModifyAdmins = function ModifyAdminsComponent() {
  const [users, setUsers] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const { setNotificationState } = useContext(NotificationContext);

  useEffect(() => {
    axios.get('http://localhost:3001/account', {
      headers: { Authorization: localStorage.getItem('token') },
    })
      .then((response) => {
        setUsers([response.data.fetchedValue].flat());
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  }, [setUsers, setNotificationState]);

  const modifyAdminRights = (idParameter) => {
    axios.put(
      'http://localhost:3001/account/admin',
      { id: idParameter },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const updatedUsers = users.map((user) => {
          const userCopy = { ...user };
          if (userCopy.id === idParameter) {
            userCopy.admin = !user.admin;
          }
          return userCopy;
        });
        setUsers(updatedUsers);
        setNotificationState(response.data.msg);
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  };

  const getUsers = (searchResultsParameter) => searchResultsParameter.map((user) => (
    <div className="account_ModifyAdmins" key={uuidv4()}>
      <p className="name_ModifyAdmins">{user.name}</p>
      <button
        type="button"
        className="adminRightsBtn_ModifyAdmins"
        onClick={() => {
          modifyAdminRights(user.id);
        }}
      >
        {`${user.admin ? 'Poista oikeudet' : 'Lisää oikeudet'}`}
      </button>
    </div>
  ));

  if (!users[0]) {
    return <Loading />;
  }

  return (
    <div className="ModifyAdmins">
      <SearchObjectsField
        searchKey="name"
        values={users}
        setResult={setSearchResults}
      />
      {getUsers(searchResults)}
    </div>
  );
};

export default ModifyAdmins;
