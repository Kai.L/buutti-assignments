import React, { useContext, useState } from 'react';
import './Book.css';
import axios from 'axios';
import PropTypes from 'prop-types';
import BookForm from './BookForm';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const Book = function BookComponent({
  id, title, year, gategory, location, gategories, locations,
}) {
  const { setNotificationState } = useContext(NotificationContext);
  const { booksState, setBooksState } = useContext(LibraryContext);
  const [modifySelected, setModifySelected] = useState(false);

  const deleteBook = (
    idParameter,
    booksStateParameter,
    setBooksStateParameter,
    setNotificationParameter,
  ) => {
    axios.delete(
      'http://localhost:3001/books',
      {
        data: { id: idParameter },
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const updatedBooks = booksStateParameter.filter((book) => book.id !== id);
        setBooksStateParameter(updatedBooks);
        setNotificationParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationParameter(e.response.data.msg);
      });
  };
  return (
    <div className="Book">
      {!modifySelected
        ? (
          <div className="book_Book">
            <div className="inputsWrapper_Book">
              <p className="title_Book">{title}</p>
              <p className="info_Book">{`Vuosi: ${year}`}</p>
              <p className="info_Book">{`Kategoria: ${gategory}`}</p>
              <p className="info_Book">{`Kirjasto: ${location}`}</p>
            </div>
            <div className="buttonsWrapper_Book">
              <button
                type="button"
                className="blueButton_Book"
                onClick={() => {
                  setModifySelected(true);
                }}
              >
                Muokkaa
              </button>
              <button
                type="button"
                className="redButton_Book"
                onClick={() => {
                  deleteBook(id, booksState, setBooksState, setNotificationState);
                }}
              >
                Poista
              </button>
            </div>
          </div>
        )
        : (
          <BookForm
            id={id}
            title={title}
            year={year}
            gategory={gategory}
            location={location}
            gategories={gategories}
            locations={locations}
            modifySelected={modifySelected}
            setModifySelected={setModifySelected}
          />
        )}
    </div>
  );
};

export default Book;

Book.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  gategory: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  gategories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  locations: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
};
