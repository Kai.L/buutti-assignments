import React, { useContext, useState } from 'react';
import './Book.css';
import PropTypes from 'prop-types';
import axios from 'axios';
import DropdownMenu from '../../../Components/DropdownMenu/DropdownMenu.jsx';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const BookForm = function BookFormComponent({
  id,
  title,
  year,
  gategory,
  location,
  gategories,
  locations,
  modifySelected,
  setModifySelected,
}) {
  const { setNotificationState } = useContext(NotificationContext);
  const { booksState, setBooksState } = useContext(LibraryContext);
  const [titleState, setTitleState] = useState(title || '');
  const [yearState, setYearState] = useState(year || null);
  const gat = gategories.find((g) => g.name === gategory);
  const loc = locations.find((l) => l.name === location);
  const [gategoryState, setGategoryState] = useState(gat || { id: null, name: null });
  const [locationState, setLocationState] = useState(loc || { id: null, name: null });

  const modifyBook = (
    idParameter,
    nameParameter,
    yearParameter,
    gategoryParameter,
    locationParameter,
    booksStateParameter,
    setBooksStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.put(
      'http://localhost:3001/books',
      {
        id: idParameter,
        name: nameParameter,
        year: yearParameter || null,
        gategory: gategoryParameter.id,
        location: locationParameter.id,
      },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const updatedBooks = booksStateParameter.map((book) => {
          const bookCopy = { ...book };
          if (bookCopy.id === id) {
            bookCopy.title = nameParameter;
            bookCopy.year = Number(yearParameter) || null;
            bookCopy.gategory = gategoryParameter ? gategoryParameter.name : null;
            bookCopy.location = locationParameter ? locationParameter.name : null;
          }
          return bookCopy;
        });
        setBooksStateParameter(updatedBooks);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };

  return (
    <div className="Book">
      <div className="book_Book">
        <div className="inputsWrapper_Book">
          <input
            className="input_Book"
            placeholder="Nimi"
            value={titleState}
            onChange={(e) => {
              setTitleState(e.target.value);
            }}
          />
          <input
            className="input_Book"
            placeholder="Vuosi"
            value={yearState}
            onChange={(e) => {
              setYearState(e.target.value);
            }}
          />
          <DropdownMenu
            selected={gategoryState}
            setSelected={setGategoryState}
            values={gategories}
            menuName="Kategoria"
          />
          <DropdownMenu
            selected={locationState}
            setSelected={setLocationState}
            values={locations}
            menuName="Kirjasto"
          />
        </div>
        <div className="buttonsWrapper_Book">
          <button
            type="button"
            className="blueButton_Book"
            onClick={() => {
              modifyBook(
                id,
                titleState,
                yearState,
                gategoryState,
                locationState,
                booksState,
                setBooksState,
                setNotificationState,
              );
            }}
          >
            Vahvista
          </button>
          <button
            type="button"
            className="redButton_Book"
            onClick={() => {
              setModifySelected(!modifySelected);
            }}
          >
            Peruuta
          </button>
        </div>
      </div>
    </div>
  );
};

export default BookForm;

BookForm.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  year: PropTypes.number.isRequired,
  gategory: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  gategories: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  locations: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  modifySelected: PropTypes.bool.isRequired,
  setModifySelected: PropTypes.func.isRequired,
};
