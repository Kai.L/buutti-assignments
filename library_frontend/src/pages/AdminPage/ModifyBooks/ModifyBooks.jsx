import React, { useContext, useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import Book from './Book.jsx';
import SearchObjectsField from '../../../Components/Search/SearchObjectsField.jsx';
import './ModifyBooks.css';
import Loading from '../../../Components/Loading/Loading.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import CreateForm from './CreateForm.jsx';

const ModifyBooks = function ModifyBooksComponent() {
  const { booksState } = useContext(LibraryContext);
  const { setNotificationState } = useContext(NotificationContext);
  const [searchResults, setSearchResults] = useState(booksState);
  const [gategories, setGategories] = useState([]);
  const [locationsState, setLocationsState] = useState([]);

  useEffect(() => {
    const getGategories = () => {
      axios.get('http://localhost:3001/gategory')
        .then((res) => {
          setGategories([res.data.fetchedValue].flat());
        })
        .catch((e) => {
          setNotificationState(e.response.data.msg);
        });
    };
    const getLocations = () => {
      axios.get('http://localhost:3001/location')
        .then((res) => {
          setLocationsState([res.data.fetchedValue].flat());
        })
        .catch((e) => {
          setNotificationState(e.response.data.msg);
        });
    };
    getLocations();
    getGategories();
  }, [setNotificationState]);

  const getBooks = () => searchResults.map((book) => (
    <Book
      key={uuidv4()}
      id={book.id}
      title={book.title}
      year={book.year}
      gategory={book.gategory}
      location={book.location}
      gategories={gategories}
      setGategories={setGategories}
      locations={locationsState}
      setLocations={setLocationsState}
    />
  ));

  if (!booksState[0] || !gategories[0] || !locationsState[0]) {
    return <Loading />;
  }

  return (
    <div className="ModifyBooks">
      <CreateForm gategories={gategories} locationsState={locationsState} />
      <SearchObjectsField
        searchKey="title"
        values={booksState}
        setResult={setSearchResults}
      />
      <div className="booksWrapper_ModifyBooks">
        {getBooks()}
      </div>
    </div>
  );
};

export default ModifyBooks;
