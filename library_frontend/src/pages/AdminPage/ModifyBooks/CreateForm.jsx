import React, { useContext, useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import './ModifyBooks.css';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import DropdownMenu from '../../../Components/DropdownMenu/DropdownMenu.jsx';

const CreateForm = function CreateFormComponent({ gategories, locationsState }) {
  const { setNotificationState } = useContext(NotificationContext);
  const { booksState, setBooksState } = useContext(LibraryContext);
  const [visibility, setVisibility] = useState(false);
  const [title, setTitle] = useState('');
  const [year, setYear] = useState('');
  const [gategory, setGategory] = useState({ id: null, name: null });
  const [location, setLocation] = useState({ id: null, name: null });

  const createNew = (
    nameParameter,
    yearParameter,
    gategoryParameter,
    locationParameter,
    booksStateParameter,
    setBooksStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.post(
      'http://localhost:3001/books',
      {
        name: nameParameter || null,
        year: yearParameter ? Number(yearParameter) : null,
        gategory: gategoryParameter.id,
        location: locationParameter.id,
      },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const createdBook = response.data.createdValue;
        const udpatedBooks = [...booksStateParameter, {
          id: createdBook.id,
          title: createdBook.name,
          gategory: gategoryParameter.name,
          location: locationParameter.name,
          year: Number(yearParameter),
          account: createdBook.account,
          to_be_returned: createdBook.to_be_returned,
        }];
        setBooksStateParameter(udpatedBooks);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationState(e.response.data.msg);
      });
  };

  return (
    <div className="CreateForm">
      {!visibility
        ? (
          <button
            type="button"
            onClick={() => {
              setVisibility(!visibility);
            }}
            className="formBtn_CreateForm"
          >
            Uusi
          </button>
        )
        : (
          <div>
            <button
              type="button"
              onClick={() => { setVisibility(!visibility); }}
              className="closeFormBtn_CreateForm"
            >
              Sulje
            </button>
            <input
              className="input_CreateForm"
              placeholder="Nimi"
              value={title}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
            />
            <input
              className="input_CreateForm"
              placeholder="Vuosi"
              value={year}
              onChange={(e) => {
                setYear(e.target.value);
              }}
            />
            <DropdownMenu
              selected={gategory}
              setSelected={setGategory}
              values={gategories}
              menuName="Kategoria"
            />
            <DropdownMenu
              selected={location}
              setSelected={setLocation}
              values={locationsState}
              menuName="Kirjasto"
            />
            <button
              type="button"
              onClick={() => {
                createNew(
                  title,
                  year,
                  gategory,
                  location,
                  booksState,
                  setBooksState,
                  setNotificationState,
                );
              }}
              className="formBtn_CreateForm"
            >
              Vahvista
            </button>
          </div>
        )}
    </div>
  );
};

export default CreateForm;

CreateForm.propTypes = {
  gategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
  locationsState: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
};
