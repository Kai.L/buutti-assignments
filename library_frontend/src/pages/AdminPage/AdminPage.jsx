import React, { useState } from 'react';
import './AdminPage.css';
import ModifyBooks from './ModifyBooks/ModifyBooks.jsx';
import ModifyGategories from './ModifyGategories/ModifyGategories.jsx';
import ModifyLocations from './ModifyLocations/ModifyLocations.jsx';
import ModifyAdmins from './ModifyAdmins/ModifyAdmins.jsx';

const AdminPage = function AdminPageComponent() {
  const [visibility, setVisibility] = useState(1);
  return (
    <div className="AdminPage">
      <div className="buttonsWrapper_AdminPage">
        <button
          type="button"
          className="btn_AdminPage"
          onClick={() => { setVisibility(1); }}
        >
          Kirjat
        </button>
        <button
          type="button"
          className="btn_AdminPage"
          onClick={() => { setVisibility(2); }}
        >
          Kategoriat
        </button>
        <button
          type="button"
          className="btn_AdminPage"
          onClick={() => { setVisibility(3); }}
        >
          Sijainnit
        </button>
        <button
          type="button"
          className="btn_AdminPage"
          onClick={() => { setVisibility(4); }}
        >
          Ylläpitäjät
        </button>
      </div>
      {visibility === 1
        ? <ModifyBooks />
        : null}
      {visibility === 2
        ? <ModifyGategories />
        : null}
      {visibility === 3
        ? <ModifyLocations />
        : null}
      {visibility === 4
        ? <ModifyAdmins />
        : null}
    </div>
  );
};

export default AdminPage;
