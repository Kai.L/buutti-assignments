import React, { useContext, useState } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const CreateForm = function CreateFormComponent({ gategories, setGategories }) {
  const { setNotificationState } = useContext(NotificationContext);
  const [visibility, setVisibility] = useState(false);
  const [gategory, setGategory] = useState('');

  const createNew = (
    nameParameter,
    gategoriesStateParameter,
    setGategoriesStateParameter,
    setNotificationStateParameter,
  ) => {
    axios.post(
      'http://localhost:3001/gategory',
      { name: nameParameter },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const udpatedGategories = [
          ...gategoriesStateParameter,
          {
            id: response.data.createdValue.id,
            name: nameParameter,
          },
        ];
        setGategoriesStateParameter(udpatedGategories);
        setNotificationStateParameter(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParameter(e.response.data.msg);
      });
  };
  return (
    <div className="CreateForm">
      {!visibility
        ? (
          <button
            type="button"
            onClick={() => {
              setVisibility(!visibility);
            }}
            className="formBtn_CreateForm"
          >
            Uusi
          </button>
        )
        : (
          <div>
            <button
              type="button"
              onClick={() => {
                setVisibility(!visibility);
              }}
              className="closeFormBtn_CreateForm"
            >
              Sulje
            </button>
            <input
              className="input_CreateForm"
              placeholder="Nimi"
              value={gategory}
              onChange={(e) => { setGategory(e.target.value); }}
            />
            <button
              type="button"
              onClick={() => {
                createNew(gategory, gategories, setGategories, setNotificationState);
              }}
              className="formBtn_CreateForm"
            >
              Vahvista
            </button>
          </div>
        )}
    </div>
  );
};

export default CreateForm;

CreateForm.propTypes = {
  gategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
  setGategories: PropTypes.func.isRequired,
};
