import React, { useContext, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import Gategory from './Gategory.jsx';
import SearchObjectsField from '../../../Components/Search/SearchObjectsField.jsx';
import Loading from '../../../Components/Loading/Loading.jsx';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';
import CreateForm from './CreateForm.jsx';

const ModifyGategories = function ModifyGategoriesComponent() {
  const [gategories, setGategories] = useState([]);
  const { setNotificationState } = useContext(NotificationContext);
  const [searchResults, setSearchResults] = useState([]);

  useEffect(() => {
    const getGategories = () => {
      axios.get('http://localhost:3001/gategory')
        .then((res) => {
          setGategories([res.data.fetchedValue].flat());
        })
        .catch((e) => {
          setNotificationState(e.response.data.msg);
        });
    };
    getGategories();
  }, [setNotificationState]);

  const getGategories = (
    gategoriesParameter,
    setGategoriesParameter,
  ) => searchResults.map((gategory) => (
    <Gategory
      key={uuidv4()}
      id={gategory.id}
      name={gategory.name}
      gategories={gategoriesParameter}
      setGategories={setGategoriesParameter}
    />
  ));

  if (!gategories[0]) {
    return <Loading />;
  }

  return (
    <div className="ModifyGategories">
      <CreateForm gategories={gategories} setGategories={setGategories} />
      <SearchObjectsField
        searchKey="name"
        values={gategories}
        setResult={setSearchResults}
      />
      {getGategories(gategories, setGategories)}
    </div>
  );
};

export default ModifyGategories;
