import './Gategory.css';
import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const Gategory = function GategoryComponent({
  id, name, gategories, setGategories,
}) {
  const { setNotificationState } = useContext(NotificationContext);
  const [modifySelected, setModifySelected] = useState(false);
  const [gategoryName, setGategoryName] = useState(name);

  const modifyGategory = (
    idParamater,
    nameParamater,
    gategoriesStateParamater,
    setGategoriesStateParamater,
    setNotificationStateParamater,
  ) => {
    axios.put(
      'http://localhost:3001/gategory',
      { id: idParamater, name: nameParamater },
      {
        headers: { Authorization: localStorage.getItem('token') },
      },
    )
      .then((response) => {
        const updatedGategories = gategoriesStateParamater.map((gategory) => {
          const gategoryCopy = { ...gategory };
          if (gategoryCopy.id === id) {
            gategoryCopy.name = name;
          }
          return gategoryCopy;
        });
        setGategoriesStateParamater(updatedGategories);
        setNotificationStateParamater(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParamater(e.response.data.msg);
      });
  };

  const deleteGategory = (
    idParamater,
    gategoriesStateParamater,
    setGategoriesStateParamater,
    setNotificationStateParamater,
  ) => {
    axios.delete('http://localhost:3001/gategory', {
      headers: { Authorization: localStorage.getItem('token') },
      data: { id: idParamater },
    })
      .then((response) => {
        const updatedGategories = gategoriesStateParamater.filter((gategory) => gategory.id !== id);
        setGategoriesStateParamater(updatedGategories);
        setNotificationStateParamater(response.data.msg);
      })
      .catch((e) => {
        setNotificationStateParamater(e.response.data.msg);
      });
  };

  return (
    <div className="Gategory">
      {!modifySelected
        ? (
          <div>
            <p className="info_Gategory">{name}</p>
            <div className="buttonsWrapper_Gategory">
              <button
                type="button"
                className="blueButton_Gategory "
                onClick={() => { setModifySelected(true); }}
              >
                Muokkaa
              </button>
              <button
                type="button"
                className="redButton_Gategory "
                onClick={() => {
                  deleteGategory(id, gategories, setGategories, setNotificationState);
                }}
              >
                Poista
              </button>
            </div>
          </div>
        )
        : (
          <div>
            <input
              className="input_Gategory"
              placeholder="Nimi"
              value={gategoryName}
              onChange={(e) => { setGategoryName(e.target.value); }}
            />
            <div className="buttonsWrapper_Gategory ">
              <button
                type="button"
                className="blueButton_Gategory "
                onClick={() => {
                  modifyGategory(id, gategoryName, gategories, setGategories, setNotificationState);
                }}
              >
                Vahvista
              </button>
              <button
                type="button"
                className="redButton_Gategory "
                onClick={() => {
                  setModifySelected(false);
                }}
              >
                Peruuta
              </button>
            </div>
          </div>
        )}
    </div>
  );
};

export default Gategory;

Gategory.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  gategories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  ).isRequired,
  setGategories: PropTypes.func.isRequired,
};
