import React from 'react';
import bearpawPic from './bearpaw.jpg';
import './FrontPage.css';

const FrontPage = function FrontPageComponent() {
  return (
    <div className="FrontPage">
      <div className="logoDiv_FrontPage">
        <p className="title_FrontPage">Karhunkäpälä kirjastot</p>
        <img alt="" className="img_FrontPage" src={bearpawPic} />
      </div>
    </div>
  );
};

export default FrontPage;
