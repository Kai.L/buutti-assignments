import React, { useState } from 'react';
import SelectedBooks from './Selected/SelectedBooks.jsx';
import BookList from './Books/BookList.jsx';
import './BooksPage.css';
import Gategories from './Gategories/Gategories.jsx';

const BooksPage = function BooksPageComponent() {
  const [selectedBooks, setSelectedBooks] = useState([]);
  const [selectedGategory, setSelectedGategory] = useState('');

  return (
    <div className="BooksPage">
      <Gategories setSelectedGategory={setSelectedGategory} />
      <div className="booksAndSelectedWrapper_BooksPage">
        <BookList
          selectedBooks={selectedBooks}
          setSelectedBooks={setSelectedBooks}
          selectedGategory={selectedGategory}
        />
        <SelectedBooks
          selectedBooks={selectedBooks}
          setSelectedBooks={setSelectedBooks}
        />
      </div>
    </div>
  );
};

export default BooksPage;
