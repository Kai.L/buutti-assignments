import React, { useContext, useState } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import './SelectedBooks.css';
import { NotificationContext } from '../../../Contexts/NotificationContext.jsx';

const SelectedBooks = function SelectedBooksComponent({ selectedBooks, setSelectedBooks }) {
  const {
    booksState, setBooksState,
  } = useContext(LibraryContext);
  const { setNotificationState } = useContext(NotificationContext);
  const [visibility, setVisibility] = useState(false);

  const getSelectedBooks = (books) => books.map((book) => (
    <button
      type="button"
      onClick={() => {
        setSelectedBooks(selectedBooks.filter((selectedBook) => book.id !== selectedBook.id));
      }}
      key={uuidv4()}
      className="selectedBook_SelectedBooks"
    >
      <p className="bookInfo_SelectedBooks">{book.title}</p>
    </button>
  ));

  const borrowBooks = (selectedBooksStateParameter, booksStateParameter) => {
    axios.all(
      selectedBooksStateParameter.map((selectedBook) => axios.put(
        'http://localhost:3001/books/borrow',
        { bookId: selectedBook.id },
        {
          headers: { Authorization: localStorage.getItem('token') },
        },
      )),
    ).then((responses) => {
      let updatedBooks = [...booksStateParameter];
      responses.forEach((response) => {
        updatedBooks = updatedBooks.map((book) => {
          const bookCopy = { ...book };
          if (bookCopy.id === response.data.fetchedValue.id) {
            bookCopy.account = response.data.fetchedValue.account;
            bookCopy.to_be_returned = response.data.fetchedValue.to_be_returned;
          }
          return bookCopy;
        });
      });
      setBooksState(updatedBooks);
      setSelectedBooks([]);
      setNotificationState(responses[0].data.msg);
    });
  };
  return (
    <div className="SelectedBooks">
      {!visibility
        ? (
          <button
            type="button"
            onClick={() => { setVisibility(!visibility); }}
            className="visibilityArrow_SelectedBooks"
          >
            <p className="visibilityArrowText_SelectedBooks">{'<'}</p>
          </button>
        )
        : (
          <div>
            <button
              type="button"
              onClick={() => { setVisibility(!visibility); }}
              className="visibilityArrow_SelectedBooks"
            >
              <p className="visibilityArrowText_SelectedBooks">{'>'}</p>
            </button>
            <div className="selectedBooksWrapper_SelectedBooks">
              {getSelectedBooks(selectedBooks)}
            </div>
            <button
              type="button"
              onClick={() => { borrowBooks(selectedBooks, booksState); }}
              className="loanAllButton_SelectedBooks"
            >
              Lainaa valitut kirjat
            </button>
            <button
              type="button"
              onClick={() => { setSelectedBooks([]); }}
              className="removeAllButton_SelectedBooks"
            >
              Poista kaikki valinnat
            </button>
          </div>
        )}
    </div>

  );
};

export default SelectedBooks;

SelectedBooks.propTypes = {
  selectedBooks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      year: PropTypes.number,
      gategory: PropTypes.string,
      location: PropTypes.string,
      to_be_returned: PropTypes.string,
    }),
  ).isRequired,
  setSelectedBooks: PropTypes.func.isRequired,
};
