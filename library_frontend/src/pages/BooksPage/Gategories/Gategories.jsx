import './Gategories.css';
import React, { useContext, useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import axios from 'axios';
import PropTypes from 'prop-types';
import { NotificationContext } from '../../../Contexts/NotificationContext';
import Loading from '../../../Components/Loading/Loading.jsx';

const Gategories = function GategoriesComponent({ setSelectedGategory }) {
  const [gategories, setGategories] = useState([]);
  const { setNotificationState } = useContext(NotificationContext);
  useEffect(() => {
    const getGategories = () => {
      axios.get('http://localhost:3001/gategory')
        .then((res) => {
          setGategories([res.data.fetchedValue].flat());
        })
        .catch((e) => {
          setNotificationState(e.response.data.msg);
        });
    };
    getGategories();
  }, [setNotificationState]);

  const getGategories = (gategoriesParameter) => gategoriesParameter.map((gategory) => (
    <button
      type="button"
      onClick={() => {
        setSelectedGategory(gategory.name);
      }}
      key={uuidv4()}
      className="gategoryButton_Gategories"
    >
      {gategory.name}
    </button>
  ));
  if (!gategories[0]) {
    return <Loading />;
  }

  return (
    <div className="Gategories">
      <button
        type="button"
        onClick={() => { setSelectedGategory(''); }}
        className="gategoryButton_Gategories"
      >
        Kaikki
      </button>
      {getGategories(gategories)}
    </div>
  );
};

export default Gategories;

Gategories.propTypes = {
  setSelectedGategory: PropTypes.func.isRequired,
};
