import React, { useContext } from 'react';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import { LibraryContext } from '../../../Contexts/LibraryContext.jsx';
import BookItem from './BookItem.jsx';
import './BookList.css';
import Loading from '../../../Components/Loading/Loading.jsx';

const BookList = function BookListComponent({
  selectedGategory,
  selectedBooks,
  setSelectedBooks,
}) {
  const { booksState } = useContext(LibraryContext);

  const getBooks = (
    books,
    selectedBooksParameter,
    setSelectedBooksParameter,
  ) => books.map((book) => {
    if ((book.gategory === selectedGategory || !selectedGategory) && !book.account) {
      return (
        <BookItem
          key={uuidv4()}
          book={book}
          selectedBooks={selectedBooksParameter}
          setSelectedBooks={setSelectedBooksParameter}
        />
      );
    }
    return null;
  });

  if (!booksState[0]) {
    return <Loading />;
  }

  return (
    <div className="BookList">
      <div className="booksWrapper_BookList">
        {getBooks(booksState, selectedBooks, setSelectedBooks)}
      </div>
    </div>
  );
};

export default BookList;

BookList.propTypes = {
  selectedGategory: PropTypes.string.isRequired,
  selectedBooks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      year: PropTypes.number,
      gategory: PropTypes.string,
      location: PropTypes.string,
      to_be_returned: PropTypes.string,
    }),
  ).isRequired,
  setSelectedBooks: PropTypes.func.isRequired,
};
