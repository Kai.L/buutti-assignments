import React from 'react';
import PropTypes from 'prop-types';
import './BookItem.css';

const BookItem = function BookItemComponent({ book, selectedBooks, setSelectedBooks }) {
  const isSelected = selectedBooks.find((selectedBook) => selectedBook.id === book.id);

  const handleSelection = (
    bookParameter,
    selectedBooksParameter,
    setSelectedBooksParameter,
    isSelectedParameter,
  ) => {
    if (isSelectedParameter) {
      setSelectedBooksParameter(
        selectedBooksParameter.filter((selectedBook) => {
          if (bookParameter.id !== selectedBook.id) {
            return true;
          }
          return false;
        }),
      );
    } else {
      setSelectedBooksParameter([...selectedBooksParameter, bookParameter]);
    }
  };

  return (
    <button
      type="button"
      onClick={() => {
        handleSelection(book, selectedBooks, setSelectedBooks, isSelected);
      }}
      className={!isSelected ? 'BookItem' : 'BookItemSelected_BookItem'}
    >
      <p className="bookTitle_BookItem">{book.title}</p>
      <div className="bookInfoWrapper_BookItem">
        <p className="bookInfo_BookItem">{`Vuosi: ${book.year}`}</p>
        <p className="bookInfo_BookItem">{`Kategoria: ${book.gategory}`}</p>
        <p className="bookInfo_BookItem">{`Kirjasto: ${book.location}`}</p>
      </div>
    </button>
  );
};

export default BookItem;

BookItem.propTypes = {
  book: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    year: PropTypes.number,
    gategory: PropTypes.string,
    location: PropTypes.string,
    to_be_returned: PropTypes.string,
  }).isRequired,
  selectedBooks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      year: PropTypes.number,
      gategory: PropTypes.string,
      location: PropTypes.string,
      to_be_returned: PropTypes.string,
    }),
  ).isRequired,
  setSelectedBooks: PropTypes.func.isRequired,
};
