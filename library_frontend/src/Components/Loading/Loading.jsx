import React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import './Loading.css';

const Loading = function LoadingComponent() {
  return (
    <div className="Loading">
      <p className="loadingText_Loading">Loading...</p>
      <CircularProgress />
    </div>
  );
};

export default Loading;
