import React, { useState, useEffect, useContext } from 'react';
import './LoginRegisterLogout.css';
import { AuthContext } from '../../../Contexts/AuthContext';

const LoginRegisterLogout = function LoginRegisterLogoutComponent() {
  const {
    authState, login, register, logout,
  } = useContext(AuthContext);
  const [visibility, setVisibility] = useState(0);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (authState.isLoggedIn) {
      setVisibility(3);
    } else {
      setVisibility(0);
    }
  }, [authState]);

  const getLoginRegisterLogoutContent = (
    visibilityParameter,
    setVisibilityParameter,
    loginParameter,
    registerParameter,
    logoutParameter,
    usernameParameter,
    setUsernameParameter,
    passwordParameter,
    setPasswordParameter,
  ) => {
    if (visibilityParameter === 0) {
      return (
        <div className="openLoginRegisterWrapper_LoginRegisterLogout">
          <button
            type="button"
            className="openLoginRegisterButton_LoginRegisterLogout"
            onClick={() => {
              setVisibilityParameter(1);
            }}
          >
            Kirjaudu
          </button>
          <button
            type="button"
            className="openLoginRegisterButton_LoginRegisterLogout"
            onClick={() => {
              setVisibilityParameter(2);
            }}
          >
            Rekisteröidy
          </button>
        </div>
      );
    }
    if (visibilityParameter === 1) {
      return (
        <div className="openLoginRegisterWrapper_LoginRegisterLogout">
          <button
            type="button"
            className="openLoginRegisterButton_LoginRegisterLogout"
            onClick={() => {
              setVisibilityParameter(0);
            }}
          >
            Sulje
          </button>
          <input
            className="input_LoginRegisterLogout"
            value={usernameParameter}
            onChange={(e) => { setUsernameParameter(e.target.value); }}
            type="text"
            placeholder="Käyttäjänimi"
          />
          <input
            className="input_LoginRegisterLogout"
            value={passwordParameter}
            onChange={(e) => { setPasswordParameter(e.target.value); }}
            type="password"
            placeholder="Salasana"
          />
          <button
            type="button"
            className="button_LoginRegisterLogout"
            onClick={() => {
              loginParameter(usernameParameter, passwordParameter);
              setUsernameParameter('');
              setPasswordParameter('');
            }}
          >
            Kirjaudu
          </button>
        </div>
      );
    }
    if (visibilityParameter === 2) {
      return (
        <div className="openLoginRegisterWrapper_LoginRegisterLogout">
          <button
            type="button"
            className="openLoginRegisterButton_LoginRegisterLogout"
            onClick={() => { setVisibilityParameter(0); }}
          >
            Sulje
          </button>
          <input
            className="input_LoginRegisterLogout"
            value={usernameParameter}
            onChange={(e) => { setUsernameParameter(e.target.value); }}
            type="text"
            placeholder="Käyttäjänimi"
          />
          <input
            className="input_LoginRegisterLogout"
            value={passwordParameter}
            onChange={(e) => { setPasswordParameter(e.target.value); }}
            type="password"
            placeholder="Salasana"
          />
          <button
            type="button"
            className="button_LoginRegisterLogout"
            onClick={() => {
              registerParameter(usernameParameter, passwordParameter);
              setUsernameParameter('');
              setPasswordParameter('');
            }}
          >
            Rekisteröidy
          </button>
        </div>
      );
    }
    return (
      <div className="openLoginRegisterWrapper_LoginRegisterLogout">
        <button
          type="button"
          className="openLoginRegisterButton_LoginRegisterLogout"
          onClick={() => {
            logoutParameter();
          }}
        >
          Kirjaudu ulos
        </button>
      </div>
    );
  };

  return (
    <div className="LoginRegisterLogout">
      {getLoginRegisterLogoutContent(
        visibility,
        setVisibility,
        login,
        register,
        logout,
        username,
        setUsername,
        password,
        setPassword,
      )}
    </div>
  );
};

export default LoginRegisterLogout;
