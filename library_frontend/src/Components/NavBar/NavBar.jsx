import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../Contexts/AuthContext';
import LoginRegisterLogout from './LoginRegisterLogout/LoginRegisterLogout';
import './NavBar.css';

const NavBar = function NavBarComponent() {
  const { authState } = useContext(AuthContext);

  const getNavBarContent = (authStateParameter) => {
    if (authStateParameter.isLoggedIn && authStateParameter.isAdmin) {
      return (
        <div className="navButtonsWrapper_NavBar">
          <Link to="/" className="navButton_NavBar">Etusivu</Link>
          <Link to="/books" className="navButton_NavBar">Kirjat</Link>
          <Link to="/account" className="navButton_NavBar">Tili</Link>
          <Link to="/admin" className="navButton_NavBar">Ylläpito</Link>
        </div>
      );
    } if (authStateParameter.isLoggedIn) {
      return (
        <div className="navButtonsWrapper_NavBar">
          <Link to="/" className="navButton_NavBar">Etusivu</Link>
          <Link to="/books" className="navButton_NavBar">Kirjat</Link>
          <Link to="/account" className="navButton_NavBar">Tili</Link>
        </div>
      );
    }
    return <div />;
  };

  return (
    <div className="NavBar">
      {getNavBarContent(authState)}
      <LoginRegisterLogout />
    </div>
  );
};

export default NavBar;
