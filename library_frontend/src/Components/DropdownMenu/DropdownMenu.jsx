import React from 'react';
import './DropdownMenu.css';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';

const DropdownMenu = function DropdownMenuComponent({
  selected, setSelected, values, menuName,
}) {
  const getSelections = (
    valuesParameter,
    popupStateParameter,
  ) => valuesParameter.map((selectionValue) => (
    <MenuItem
      onClick={() => {
        popupStateParameter.close();
        setSelected(selectionValue);
      }}
      key={uuidv4()}
    >
      {selectionValue.name}
    </MenuItem>
  ));

  return (
    <div className="DropdownMenu">
      <PopupState variant="popover" popupId="demo-popup-menu">
        {(popupState) => (
          <>
            <Button className="btn_DropdownMenu" variant="contained" {...bindTrigger(popupState)}>
              {selected.name ? selected.name : menuName}
            </Button>
            <Menu {...bindMenu(popupState)}>
              {getSelections(values, popupState)}
            </Menu>
          </>
        )}
      </PopupState>
    </div>
  );
};

export default DropdownMenu;

DropdownMenu.propTypes = {
  selected: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  }).isRequired,
  setSelected: PropTypes.func.isRequired,
  values: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
  menuName: PropTypes.string.isRequired,
};
