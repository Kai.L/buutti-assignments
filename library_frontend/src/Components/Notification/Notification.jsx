import React, { useContext } from 'react';
import './Notification.css';
import { NotificationContext } from '../../Contexts/NotificationContext';

const Notification = function NotificationComponent() {
  const { notificationState } = useContext(NotificationContext);

  return (
    <div className="Notification">
      {notificationState
        ? <p className="notificationText_Notification">{notificationState}</p>
        : null}
    </div>
  );
};

export default Notification;
