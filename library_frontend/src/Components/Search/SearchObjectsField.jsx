import React, { useEffect, useRef } from 'react';
import './SearchObjectsField.css';
import PropTypes from 'prop-types';

const SearchObjectsField = function SearchObjectsFieldComponent({ searchKey, values, setResult }) {
  const searchRef = useRef('');

  useEffect(() => {
    setResult(values.filter((value) => {
      if (value[searchKey]) {
        return value[searchKey].toLowerCase().startsWith(
          searchRef.current.toLowerCase(),
        );
      }
      return null;
    }));
  }, [searchKey, values, setResult]);

  const handleSearch = (searchRefParameter) => {
    const searchResult = values.filter(
      (value) => {
        if (value[searchKey]) {
          return value[searchKey].toLowerCase().startsWith(
            searchRefParameter.current.toLowerCase(),
          );
        }
        return null;
      },
    );
    setResult(searchResult);
  };

  return (
    <div className="SearchObjectsField">
      <input
        className="searchInput_SearchObjectsField"
        placeholder="Haku"
        value={searchRef.current}
        onChange={(e) => {
          searchRef.current = e.target.value;
          handleSearch(searchRef);
        }}
      />
    </div>
  );
};

export default SearchObjectsField;

SearchObjectsField.propTypes = {
  searchKey: PropTypes.string.isRequired,
  values: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })),
    PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
    })),
  ]).isRequired,
  setResult: PropTypes.func.isRequired,
};
