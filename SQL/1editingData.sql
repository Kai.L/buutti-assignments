UPDATE borrows
SET due_date = due_date + interval '2 days'
WHERE id = 1;

INSERT INTO books (name, release_year, genre_id, language_id)
VALUES
	('Miekka ja kynä', 1988, 3, 1),
	('Ase ja mies', 1997, 2, 2);