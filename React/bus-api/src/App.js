import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './App.css';

const App = () => {

	const [busData, setBusData] = useState([]);

	useEffect(() => {
		const getBusData = async () => {
			const response = await axios.get('https://data.itsfactory.fi/journeys/api/1/lines');
			setBusData(response.data.body);
		};
		getBusData();
	}, []);

	return (
		<div className="App">
			<table>
				<tbody>
					{busData.map((d, i) => {
						return (
							<tr key={i} >
								<td>{d.name}</td>
								<td>{d.description}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div >
	);
};

export default App;
