
import './Clockapp.css';
import { useState } from 'react';

const Clockapp = () => {
  const [time, setTime] = useState(new Date())

  setTimeout(() => {
    setTime(new Date())
  }, 1000)

  return (

    <div className="Clockapp">
      <p className="title">Time</p>
      <p className="time">{`${time.getHours().toString().length < 2 ? `0${time.getHours()}` : time.getHours()}:${time.getMinutes().toString().length < 2 ? `0${time.getMinutes()}` : time.getMinutes()}.${time.getSeconds().toString().length < 2 ? `0${time.getSeconds()}` : time.getSeconds()}`}</p>
      <p className="title">Date</p>
      <p className="date">{`${time.getDate()}.${time.getMonth() + 1}.${time.getFullYear()}`}</p>
    </div >
  )
}

export default Clockapp;
