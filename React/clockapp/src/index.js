import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Clockapp from './Clockapp';

ReactDOM.render(
  <React.StrictMode>
    <Clockapp />
  </React.StrictMode>,
  document.getElementById('root')
);
