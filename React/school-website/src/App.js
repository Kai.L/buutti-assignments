/* eslint-disable react/prop-types */
import React, { createContext, useContext, useReducer, useState } from 'react';
import './App.css';
import labelsFIN from './labelsFIN.json';
import labelsEN from './labelsEN.json';

const LanguageContext = createContext([{}, () => { }]);

const LanguageProvider = (props) => {
	const [state, setState] = useState(labelsFIN);
	return (
		<LanguageContext.Provider value={[state, setState]}>
			{props.children}
		</LanguageContext.Provider>
	);
};

const App = () => {
	return <LanguageProvider><InfoForm /></LanguageProvider>;
};

const reducer = (state, action) => {
	switch (action.type) {
	case 'NAME':
		return { ...state, name: action.data };
	case 'NATIONALITY':
		return { ...state, nationality: action.data };
	case 'ADDRESS':
		return { ...state, address: action.data };
	case 'FIELD_OF_STUDY':
		return { ...state, field_of_study: action.data };
	default:
		return state;
	}
};
const reducer2 = (state, action) => {
	switch (action.type) {
	case 'SAVE':
		return action.data;
	default:
		return state;
	}
};

const InfoForm = () => {
	const [state, setState] = useContext(LanguageContext);
	const [studentInfo, dispatch] = useReducer(reducer,
		{
			name: '',
			nationality: '',
			address: '',
			field_of_study: ''
		}
	);
	const [savedStudentInfo, dispatchSave] = useReducer(reducer2, {});

	const changeLanguage = () => {
		if (state === labelsEN) {
			setState(labelsFIN);
		} else {
			setState(labelsEN);
		}
	};
	return (
		<div className="InfoForm">
			<h1>{state.header}</h1>
			<form className="studentInfoForm">
				<div className="inputWrapper">
					<label htmlFor="nameInput">{state.name}</label>
					<input className="nameInput" value={studentInfo.name} onChange={(e) => { dispatch({ type: 'NAME', data: e.target.value }); }} />
				</div>
				<div className="inputWrapper">
					<label htmlFor="nationalityInput">{state.nationality}</label>
					<input className="nationalityInput" value={studentInfo.nationality} onChange={(e) => { dispatch({ type: 'NATIONALITY', data: e.target.value }); }} />
				</div>
				<div className="inputWrapper">
					<label htmlFor="addressInput">{state.address}</label>
					<input className="addressInput" value={studentInfo.address} onChange={(e) => { dispatch({ type: 'ADDRESS', data: e.target.value }); }} />
				</div>
				<div className="inputWrapper">
					<label htmlFor="field_of_studyInput">{state.field_of_study}</label>
					<input className="field_of_studyInput" value={studentInfo.field_of_study} onChange={(e) => { dispatch({ type: 'FIELD_OF_STUDY', data: e.target.value }); }} />
				</div>
			</form>
			<button className="saveButton" onClick={() => { dispatchSave({ type: 'SAVE', data: studentInfo }); }}>{state.save}</button>
			<div className="studentInfoWrapper">
				<p className="studentInfo"><span>{state.name}</span>{savedStudentInfo.name}</p>
				<p className="studentInfo"><span>{state.nationality}</span>{savedStudentInfo.nationality}</p>
				<p className="studentInfo"><span>{state.address}</span>{savedStudentInfo.address}</p>
				<p className="studentInfo"><span>{state.field_of_study}</span>{savedStudentInfo.field_of_study}</p>
			</div>
			<button className="changeLangButton" onClick={changeLanguage}>{state.changeLanguage}</button>
		</div >
	);
};

export default App;
