
import './App.css';
import React, { useState } from 'react';

function App() {

	const [numbers, setNumbers] = useState([]);

	const addRandomNumber = () => {
		const randomNumber = Math.floor((Math.random() * 70) + 1);
		if (numbers.includes(randomNumber)) {
			addRandomNumber();
		} else {
			setNumbers((prev) => [...prev, randomNumber]);
		}
	};

	const resetNumbers = () => {
		setNumbers([]);
	};

	return (
		<div className="App">
			{numbers.map((b) => {
				return (<div key={b} className="ball">{b}</div>);
			})}
			<button className="button" onClick={addRandomNumber}>+</button>
			<button className="resetButton" onClick={resetNumbers}>Reset</button>
		</div>
	);
}

export default App;
