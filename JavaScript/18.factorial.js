
const factorial = (n) => {
	let result = n;
	let i = n - 1;
	while (i > 0) {
		result = result * i;
		i--;
	}
	return result;
};
console.log(factorial(5));