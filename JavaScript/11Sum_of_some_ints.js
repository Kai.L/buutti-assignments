const n = 17;
let sum = 0;
for (let i = 0; i <= n; i++) {
	if (i % 3 === 0 || i % 5 === 0) {
		sum = sum + i;
	}
}
console.log(sum);

sum = 0;
let i = 0;
while (i <= n) {
	if (i % 3 === 0 || i % 5 === 0) {
		sum = sum + i;
	}
	i++;
}
console.log(sum);