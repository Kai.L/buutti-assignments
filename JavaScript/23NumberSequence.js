const numberSequence = (n) => {
	const arr = [1, 1];
	for (let i = 1; i < n; i++) {
		arr.push((arr[i] + arr[i - 1]));
	}
	arr.pop();
	return arr;
};
console.log(numberSequence(9));