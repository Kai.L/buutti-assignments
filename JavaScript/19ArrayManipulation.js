const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];

const divisibleBy3 = arr.filter(n => n % 3 === 0);

console.log(divisibleBy3);

const multipliedBy2 = arr.map(n => n = n * 2);

console.log(multipliedBy2);

const sumOfAllValues = arr.reduce((accumulator, n) => accumulator + n, 0);

console.log(sumOfAllValues);