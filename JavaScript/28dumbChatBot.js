const readline = require("readline-sync");

console.log(">Hi! I am a dumb chat bot");
console.log(">You can check all the things I can do by typing 'help'.");

let questionCounter = 0;
let botName = "dumbBot";
let command = "";
const weather = {
	temperature: [21, 37, 107],
	cloudy: ["yes", "no", "maybe"],
	sunny: ["yes", "no", "maybe"],
	wind: ["yes", "no", "maybe"]
};

const renameBot = () => {
	const newName = readline.question(">Type my new name, please.");
	const isHappy = readline.question(">Are you happy with the name " + `${newName}` + "?");
	if (isHappy === "yes") {
		botName = newName;
		console.log(">I was renamed to " + `${botName}`);
	} else if (isHappy === "no") {
		console.log(`Name not changed. My name is ${botName}`);
	}
	questionCounter += 1;
};

const getForecast = () => {
	console.log(">Tomorrows weather will be....");
	console.log(">Temperature: " + weather.temperature[Math.floor(Math.random() * weather.temperature.length)]);
	console.log(">Cloudy: " + weather.cloudy[Math.floor(Math.random() * weather.cloudy.length)]);
	console.log(">Sunny: " + weather.sunny[Math.floor(Math.random() * weather.sunny.length)]);
	console.log(">Wind: " + weather.wind[Math.floor(Math.random() * weather.wind.length)]);
	questionCounter += 1;
};



while (command !== "quit") {
	command = readline.question(">");
	switch (command) {
	case "help": {
		console.log("Here´s a list of commands that I can execute!\n\nhelp: Opens this dialog.\nhello: I will say hello to you\nbotInfo: I will introduce myself\nbotName: I will tell my name\nbotRename: You can rename me\nforecast: I will forecast tomorrows weather 100 % accurately\nquit: Quits the program.");
		questionCounter += 1;
		break;
	}
	case "hello": {
		const name = readline.question(">What is your name?");
		console.log(`>Hello there, ${name}!`);
		questionCounter += 1;
		break;
	}
	case "botInfo": {
		console.log(">I am a dumb bot. You can ask me almost anything :). You have already asked me " + `${questionCounter}` + " questions.");
		questionCounter += 1;
		break;
	}
	case "botName": {
		console.log(">My name is currently " + `${botName}` + ". If you want to change it, type botRename.");
		questionCounter += 1;
		break;
	}
	case "botRename": {
		renameBot();
		break;
	}
	case "forecast": {
		getForecast();
		break;
	}
	default:
		break;
	}
}
