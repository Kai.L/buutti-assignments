
const getRunway = (degrees = 303) => {
	if (degrees < 10) {
		return 1;
	} else {
		const roundable = degrees % 10;
		degrees = degrees - roundable;
		if (roundable / 5 >= 1) {
			return (degrees + 10) / 10;
		} else {
			return degrees / 10;
		}
	}
};
console.log("Runway " + getRunway());