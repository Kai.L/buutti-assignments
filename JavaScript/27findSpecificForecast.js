const fs = require("fs");

const forecast = {
	day: "tuesday",
	temperature: 20,
	cloudy: true,
	sunny: false,
	windy: false,
};
fs.writeFileSync("forecast_data.json", JSON.stringify([forecast]), (err) => {
	if (err) {
		console.log("Could not save forecasts to file");
	}
});
try {
	const data = fs.readFileSync("forecast_data.json");
	const forecasts = JSON.parse(data);
	forecasts.forEach((f, i) => {
		if (f.day === "tuesday") {
			forecasts[i].temperature = 45;
		}
	});
	fs.writeFileSync("forecast_data.json", JSON.stringify(forecasts), (err) => {
		if (err) {
			console.log("Could not save forecasts to file");
		}
	});
} catch (e) {
	console.log(e);
}

