
const countDownNotification = (time, func) => {
	setTimeout(() => {
		func();
	}, time);
};

countDownNotification(1000, () => {
	console.log("Wait 1 second");
	countDownNotification(1000, () => {
		console.log("Wait another second");
		countDownNotification(1000, () => {
			console.log("Wait the last 1 second");
		});
	});
});
