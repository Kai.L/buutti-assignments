


const logWaitTime = new Promise(() => {
	setTimeout(() => {
		console.log("Wait 1 second");
		setTimeout(() => {
			console.log("Wait another second");
			setTimeout(() => {
				console.log("Wait the last second..");
			}, 1000);
		}, 1000);
	}, 1000);
});
logWaitTime.then((val) => {
	console.log(val);
});