const str = "aabbooooofffkkccjdddTTT";

const characterArr = str.split("");
const firstNonRepeating = characterArr.find((c, i, arr) => {
	if (arr[i - 1] !== c && arr[i + 1] !== c) {
		return true;
	}
});
console.log(firstNonRepeating);