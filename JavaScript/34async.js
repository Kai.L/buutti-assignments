const getValue = function () {
	return new Promise((res) => {
		setTimeout(() => {
			res({ value: Math.random() });
		}, Math.random() * 1500);
	});
};

const printValues1 = () => {
	let value1Then = 0;
	let value2Then = 0;
	getValue().then((val) => {
		value1Then = val.value;
	}).then(() => {
		getValue().then((val) => {
			value2Then = val.value;
		}).then(() => {
			console.log(`Value 1 is ${value1Then} and value 2 is ${value2Then}`);
		});
	});
};
const printValues2 = async () => {
	let value1Async = 0;
	let value2Async = 0;
	value1Async = await getValue();
	value2Async = await getValue();
	console.log(`Value 1 is ${value1Async.value} and value 2 is ${value2Async.value}`);
};
printValues1();
printValues2();