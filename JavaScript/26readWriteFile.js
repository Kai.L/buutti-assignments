const fs = require("fs");
const readStream = fs.createReadStream("./textFile.txt", "utf-8");
let newContentArr = [];
readStream.on("data", (txt) => {
	let wordsArr = txt.toString().split(" ");
	newContentArr = wordsArr.map(w => {
		if (w.toLowerCase() === "joulu") {
			return "kinkku";
		} else if (w.toLowerCase() === "lapsilla") {
			return "poroilla";
		}
		return w;
	});

	let newContent = "";
	newContentArr.forEach((w) => {
		newContent = newContent + w + " ";
	});


	const stream = fs.createWriteStream("./textFile.txt");
	stream.write(newContent, (err) => {
		if (err) console.log(err);
		else console.log("success");
	});
});
