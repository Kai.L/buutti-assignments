const students = [
	{ name: "markku", score: 99 }, { name: "karoliina", score: 58 },
	{ name: "susanna", score: 69 }, { name: "benjamin", score: 77 },
	{ name: "isak", score: 49 }, { name: "liisa", score: 89 },
];

const highestScoringPerson = { name: "", score: 0 };
const lowestScoringPerson = { name: students[0].name, score: students[0].score };
let studentsTotalScore = 0;

for (let i = 0; students.length > i; i++) {
	if (students[i].score > highestScoringPerson.score) {
		highestScoringPerson.score = students[i].score;
		highestScoringPerson.name = students[i].name;
	}
	if (students[i].score < lowestScoringPerson.score) {
		lowestScoringPerson.score = students[i].score;
		lowestScoringPerson.name = students[i].name;
	}
	studentsTotalScore += students[i].score;
}
const studentsAvgScore = studentsTotalScore / students.length;

for (let i = 0; students.length > i; i++) {
	if (students[i].score > studentsAvgScore) {
		console.log(students[i]);
	}
}

for (let i = 0; students.length > i; i++) {
	const student = students[i];
	if (student.score <= 39) {
		students[i].grade = "1";
	} else if (student.score <= 59) {
		students[i].grade = "2";
	} else if (student.score <= 79) {
		students[i].grade = "3";
	} else if (student.score <= 94) {
		students[i].grade = "4";
	} else if (student.score <= 100) {
		students[i].grade = "5";
	}
}
