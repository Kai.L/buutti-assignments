const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

let largestNumber = 0;

for (let i = 0; i < arr.length; i++) {
	if (arr[i] > largestNumber) {
		largestNumber = arr[i];
	}
}
console.log(largestNumber);

let secondLargestNumber = 0;

for (let i = 0; i < arr.length; i++) {
	if (arr[i] > secondLargestNumber && arr[i] < largestNumber) {
		secondLargestNumber = arr[i];
	}
}
console.log(secondLargestNumber);