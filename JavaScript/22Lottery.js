const lotteryNumbers = [];

const getRandomNumber = () => {
	const number = Math.floor(Math.random() * 40) + 1;
	if (lotteryNumbers.includes(number)) {
		getRandomNumber();
	} else {
		lotteryNumbers.push(number);
	}
};
for (let i = 0; i < 7; i++) {
	getRandomNumber();
}

for (let i = 0; i < lotteryNumbers.length; i++) {
	setTimeout(() => {
		console.log(lotteryNumbers[i]);
	}, i * 1000);
}