import crypto from 'crypto';
import jsonwebtoken from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const pathToKey = path.join(__dirname, '..', 'id_rsa_priv.pem');
const PRIV_KEY = fs.readFileSync(pathToKey, 'utf8');


export const getSaltHashPassword = (password) => {
	const salt = crypto.randomBytes(32).toString('hex');
	const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');

	return { salt: salt, hash: hash };
};
export const validatePassword = (password, hash, salt) => {
	const hashToVerify = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex');

	return hash === hashToVerify;
};
export const issueJWT = (user) => {
	const id = user.id;
	const expiresIn = '1d';
	const payload = {
		sub: id,
		iat: Date.now(),
		admin: user.admin
	};
	const signedToken = jsonwebtoken.sign(payload, PRIV_KEY, { expiresIn: expiresIn, algorithm: 'RS256' });

	return {
		token: 'Bearer ' + signedToken,
		expires: expiresIn
	};
};
export const isAdmin = (req, res, next) => {
	const admin = req.user.admin;
	if (admin) {
		next();
	} else {
		res.status(401).json({ success: false, msg: 'You are not authorized.' });
	}
};

export default {
	getSaltHashPassword,
	validatePassword,
	issueJWT,
	isAdmin
};