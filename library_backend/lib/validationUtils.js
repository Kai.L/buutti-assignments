

export const validateStringTypeLength = (value, min, max) => {
	return (value && typeof value === 'string' && value.length >= min && value.length <= max)
		? true
		: false;
};

export const validateNumber = (value) => {
	return (value && typeof value === 'number')
		? true
		: false;
};

export const validateNoSpecialCharacters = (value) => {
	return (value && value.match(/^[0-9a-zA-Z]+$/))
		? true
		: false;
};

export const validatePassword = (value) => {
	return (value && value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,30}$/))
		? true
		: false;
};

export const validateOwnAccount = (id, authId) => {
	return (id === authId)
		? true
		: false;
};

export default {
	validateStringTypeLength,
	validateNumber,
	validateNoSpecialCharacters,
	validatePassword,
	validateOwnAccount
};