import pg from 'pg';
const { Pool } = pg;

export const db = new Pool({
	user: 'kai',
	host: 'localhost',
	database: 'library',
	password: 'password',
	port: 5432,
});

export default db;