import { Strategy as JwtStrategy } from 'passport-jwt';
import { ExtractJwt } from 'passport-jwt';
import { readFileSync } from 'fs';
import { join } from 'path';
import { db } from './db.js';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const pathToKey = join(__dirname, '..', 'id_rsa_pub.pem');
const PUB_KEY = readFileSync(pathToKey, 'utf8');

const options = {
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
	secretOrKey: PUB_KEY,
	algorithms: ['RS256']
};

const strategy = new JwtStrategy(options, (payload, done) => {
	db.query('SELECT * FROM account WHERE id = $1', [payload.sub])
		.then((result) => {
			const user = result.rows[0];
			return user ? done(null, user) : done(null, false);
		})
		.catch((e) => {
			console.log(e);
		});
});

export default (passport) => {
	passport.use(strategy);
};