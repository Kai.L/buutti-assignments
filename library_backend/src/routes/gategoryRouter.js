import express from 'express';
import passport from 'passport';
import { getGategories, updateGategory, addGategory, deleteGategory } from '../controllers/gategoryController.js';
import { isAdmin } from '../../lib/authUtils.js';

const gategoryRouter = express.Router();

gategoryRouter.get('/gategory', getGategories);
gategoryRouter.put('/gategory', passport.authenticate('jwt', { session: false }), isAdmin, updateGategory);
gategoryRouter.post('/gategory', passport.authenticate('jwt', { session: false }), isAdmin, addGategory);
gategoryRouter.delete('/gategory', passport.authenticate('jwt', { session: false }), isAdmin, deleteGategory);

export default gategoryRouter;