import express from 'express';
import accountRouter from './accountRouter.js';
import booksRouter from './booksRouter.js';
import gategoryRouter from './gategoryRouter.js';
import locationRouter from './locationRouter.js';

const router = express.Router();

router.use(accountRouter);
router.use(booksRouter);
router.use(gategoryRouter);
router.use(locationRouter);

export default router;
