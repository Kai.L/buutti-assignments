import express from 'express';
import passport from 'passport';
import { getBooks, addBook, updateBook, deleteBook, borrowBook, returnBook } from '../controllers/bookController.js';
import { isAdmin } from '../../lib/authUtils.js';

const booksRouter = express.Router();

booksRouter.get('/books', passport.authenticate('jwt', { session: false }), getBooks);
booksRouter.post('/books', passport.authenticate('jwt', { session: false }), isAdmin, addBook);
booksRouter.put('/books', passport.authenticate('jwt', { session: false }), isAdmin, updateBook);
booksRouter.delete('/books', passport.authenticate('jwt', { session: false }), isAdmin, deleteBook);
booksRouter.put('/books/borrow', passport.authenticate('jwt', { session: false }), borrowBook);
booksRouter.put('/books/return', passport.authenticate('jwt', { session: false }), returnBook);

export default booksRouter;