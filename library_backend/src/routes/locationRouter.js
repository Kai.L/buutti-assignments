import express from 'express';
import passport from 'passport';
import { getLocations, updateLocation, addLocation, deleteLocation } from '../controllers/locationController.js';
import { isAdmin } from '../../lib/authUtils.js';

const locationRouter = express.Router();

locationRouter.get('/location', getLocations);
locationRouter.put('/location', passport.authenticate('jwt', { session: false }), isAdmin, updateLocation);
locationRouter.post('/location', passport.authenticate('jwt', { session: false }), isAdmin, addLocation);
locationRouter.delete('/location', passport.authenticate('jwt', { session: false }), isAdmin, deleteLocation);

export default locationRouter;