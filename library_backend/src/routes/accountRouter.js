import express from 'express';
import passport from 'passport';
import { getAccounts, createAccount, deleteAccount, modifyAdminStatus } from '../controllers/accountController.js';
import { login } from '../controllers/loginController.js';
import { isAdmin } from '../../lib/authUtils.js';

const accountRouter = express.Router();

accountRouter.get('/account', passport.authenticate('jwt', { session: false }), isAdmin, getAccounts);
accountRouter.post('/account', createAccount);
accountRouter.delete('/account', passport.authenticate('jwt', { session: false }), deleteAccount);
accountRouter.post('/account/login', login);
accountRouter.put('/account/admin', passport.authenticate('jwt', { session: false }), isAdmin, modifyAdminStatus);

export default accountRouter;