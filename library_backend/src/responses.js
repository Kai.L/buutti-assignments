

export const createStatusOKResponse = (response, message, fetchedObject) => {
	response.status(200).json({ success: true, msg: message, fetchedValue: fetchedObject });
};

export const createCreatedResponse = (response, message, createdObject) => {
	response.status(201).json({ success: true, msg: message, createdValue: createdObject });
};

export const createUnAuthorizedResponse = (response, message) => {
	response.status(401).json({ success: false, msg: message });
};

export const createForbiddenResponse = (response, message) => {
	response.status(403).json({ success: false, msg: message });
};

export const createInternalServerErrorResponse = (response) => {
	response.status(500).json({ success: false, msg: 'Something went wrong.' });
};

export default {
	createStatusOKResponse,
	createCreatedResponse,
	createUnAuthorizedResponse,
	createForbiddenResponse,
	createInternalServerErrorResponse
};