import { db } from '../../config/db.js';
import { createStatusOKResponse, createInternalServerErrorResponse, createForbiddenResponse, createCreatedResponse } from '../responses.js';
import { validateStringTypeLength, validateNumber } from '../../lib/validationUtils.js';

export const getGategories = (request, response) => {
	db.query('SELECT * FROM gategory')
		.then((result) => {
			createStatusOKResponse(response, 'Successfully fetched gategories.', result.rows);
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export const addGategory = (request, response) => {
	const { name } = request.body;
	!validateStringTypeLength(name, 1, 25) ?
		createForbiddenResponse(response, 'You need to fill all fields correctly.')
		:
		db.query('INSERT INTO gategory (name) VALUES ($1) RETURNING *', [name])
			.then((result) => {
				createCreatedResponse(response, 'Successfully added gategory.', result.rows[0]);
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
};

export const updateGategory = (request, response) => {
	const { id, name } = request.body;
	!validateStringTypeLength(name, 1, 25) || !validateNumber(id) ?
		createForbiddenResponse(response, 'You need to fill all fields correctly.')
		:
		db.query('UPDATE gategory SET name = $2 WHERE id = $1 RETURNING *', [id, name])
			.then((result) => {
				createStatusOKResponse(response, 'Successfully update gategory.', result.rows[0]);
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
};

export const deleteGategory = (request, response) => {
	const gategoryId = parseInt(request.body.id);
	db.query('SELECT * FROM books WHERE gategory = $1', [gategoryId])
		.then((result) => {
			result.rows[0] ?
				createForbiddenResponse(response, 'Cannot delete gategory while books are linked to it.')
				:
				db.query('DELETE FROM gategory WHERE id = $1 RETURNING *', [gategoryId])
					.then((result) => {
						createStatusOKResponse(response, 'Successfully deleted gategory.', result.rows[0]);
					}).catch(() => {
						createInternalServerErrorResponse(response);
					});
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export default {
	getGategories,
	addGategory,
	updateGategory,
	deleteGategory
};