import { db } from '../../config/db.js';
import { createStatusOKResponse, createCreatedResponse, createUnAuthorizedResponse, createForbiddenResponse, createInternalServerErrorResponse } from '../responses.js';
import { getSaltHashPassword, issueJWT } from '../../lib/authUtils.js';
import { validateStringTypeLength, validateNoSpecialCharacters, validatePassword, validateOwnAccount } from '../../lib/validationUtils.js';

export const getAccounts = (request, response) => {
	db.query('SELECT * FROM account')
		.then((result) => {
			createStatusOKResponse(response, 'Successfully fetched accounts.', result.rows);
		});
};

export const createAccount = (request, response) => {
	const { name } = request.body;
	const { password } = request.body;

	if (!validateStringTypeLength(name, 3, 16) || !validateNoSpecialCharacters(name)) {
		createForbiddenResponse(response, 'Username needs to be 3-16 characters long (a-z, A-Z, 0-9).');
	} else if (!validatePassword(password)) {
		createForbiddenResponse(response, 'Password needs to be 8-30 characters long (a-z, A-Z, 0-9, !@#$%^&).');
	} else {
		const { salt, hash } = getSaltHashPassword(password);

		db.query('SELECT name FROM account WHERE name = $1', [name])
			.then((result) => {
				result.rows[0] ?
					createForbiddenResponse(response, 'User already exists.')
					:
					db.query('INSERT INTO account (name, admin, salt, hash) VALUES ($1, $2, $3, $4) RETURNING *', [name, false, salt, hash])
						.then((result) => {
							const jwt = issueJWT(result.rows[0]);
							createCreatedResponse(response, 'User created.', { user: result.rows[0], token: jwt.token, expiresIn: jwt.expires });
						}).catch(() => {
							createInternalServerErrorResponse(response);
						});
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
	}
};

export const deleteAccount = (request, response) => {
	const requestAccountId = parseInt(request.body.id);
	const authUserId = parseInt(request.user.id);
	if (validateOwnAccount(requestAccountId, authUserId)) {
		db.query('SELECT * FROM books WHERE account = $1', [requestAccountId])
			.then((result) => {
				result.rows[0] ?
					createForbiddenResponse(response, 'Cannot delete account while books are linked to it.')
					:
					db.query('DELETE FROM account WHERE id = $1', [requestAccountId])
						.then(() => {
							createStatusOKResponse(response, 'Account deleted.');
						}).catch(() => {
							createInternalServerErrorResponse(response);
						});
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
	} else {
		createUnAuthorizedResponse(response, 'Not authorized.');
	}
};

export const modifyAdminStatus = (request, response) => {
	const { id } = request.body;
	db.query('SELECT admin FROM account WHERE id = $1', [id])
		.then((result) => {
			const isAdmin = result.rows[0].admin;
			db.query('UPDATE account SET admin = $2 WHERE id = $1', [id, !isAdmin])
				.then(() => {
					createStatusOKResponse(response, 'Admin rights modified.');
				}).catch(() => {
					createInternalServerErrorResponse(response);
				});
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export default {
	createAccount,
	deleteAccount,
	modifyAdminStatus
};