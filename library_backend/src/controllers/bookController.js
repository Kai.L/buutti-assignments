import { db } from '../../config/db.js';
import { validateStringTypeLength, validateNumber } from '../../lib/validationUtils.js';
import { createStatusOKResponse, createCreatedResponse, createForbiddenResponse, createInternalServerErrorResponse } from '../responses.js';

export const getBooks = (request, response) => {
	db.query('SELECT B.id AS id, B.name AS title, B.year AS year, G.name AS gategory, L.Name AS location, A.id AS account, B.to_be_returned AS to_be_returned FROM books AS B LEFT JOIN gategory AS G ON B.gategory = G.id LEFT JOIN account AS A ON B.account = A.id LEFT JOIN location AS L ON B.location = L.id')
		.then((result) => {
			createStatusOKResponse(response, 'Successfully fetched books', result.rows);
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export const addBook = (request, response) => {
	const { name, year, gategory, location } = request.body;
	!validateStringTypeLength(name, 1, 100) || !validateNumber(year) || !validateNumber(gategory) || !validateNumber(location) ?
		createForbiddenResponse(response, 'You need to fill all fields correctly.')
		:
		db.query('INSERT INTO books (name, year, gategory, location) VALUES ($1, $2, $3, $4) RETURNING *', [name, year, gategory, location])
			.then((result) => {
				createCreatedResponse(response, 'Successfully added book.', result.rows[0]);
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});

};

export const updateBook = (request, response) => {
	const { id, name, year, gategory, location } = request.body;
	db.query('UPDATE books SET name = COALESCE($2, name), year = COALESCE($3, year), gategory = COALESCE($4, gategory), location = COALESCE($5, location) WHERE id = $1 RETURNING *', [id, name, year, gategory, location])
		.then((result) => {
			createStatusOKResponse(response, 'Successfully updated book.', result.rows[0]);
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export const deleteBook = (request, response) => {
	const id = parseInt(request.body.id);
	db.query('SELECT * FROM books WHERE id = $1', [id])
		.then((result) => {
			result.rows[0].account ?
				createForbiddenResponse(response, 'Cannot delete book while it is borrowed.', result.rows[0])
				:
				db.query('DELETE FROM books WHERE id = $1 RETURNING *', [id])
					.then((result) => {
						createStatusOKResponse(response, 'Successfully deleted book.', result.rows[0]);
					}).catch(() => {
						createInternalServerErrorResponse(response);
					});
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});

};

export const borrowBook = (request, response) => {
	const bookId = parseInt(request.body.bookId);
	const borrowerId = parseInt(request.user.id);
	const date = new Date(Date.now());
	date.setDate(date.getDate() + 7);
	const returnDate = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;

	db.query('SELECT * FROM books WHERE id = $1', [bookId])
		.then((result) => {
			result.rows[0].account ?
				createForbiddenResponse(response, 'Cannot borrow book while it is borrowed.', result.rows[0])
				:
				db.query('UPDATE books SET account = $1, to_be_returned = $2 WHERE id = $3 RETURNING *', [borrowerId, returnDate, bookId])
					.then((result) => {
						createStatusOKResponse(response, 'Successfully borrowed books.', result.rows[0]);
					}).catch(() => {
						createInternalServerErrorResponse(response);
					});
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});

};

export const returnBook = (request, response) => {
	const bookId = parseInt(request.body.bookId);
	db.query('UPDATE books SET account = $1, to_be_returned = $2 WHERE id = $3 RETURNING *', [null, null, bookId])
		.then((result) => {
			createStatusOKResponse(response, 'Successfully returned book.', result.rows[0]);
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export default {
	getBooks,
	addBook,
	updateBook,
	deleteBook,
	borrowBook,
	returnBook
};