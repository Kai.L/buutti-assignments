import { db } from '../../config/db.js';
import { createStatusOKResponse, createInternalServerErrorResponse, createForbiddenResponse, createCreatedResponse } from '../responses.js';
import { validateStringTypeLength, validateNumber } from '../../lib/validationUtils.js';

export const getLocations = (request, response) => {
	db.query('SELECT * FROM location')
		.then((result) => {
			createStatusOKResponse(response, 'Successfully fetched locations.', result.rows);
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export const addLocation = (request, response) => {
	const { name } = request.body;
	!validateStringTypeLength(name, 1, 40) ?
		createForbiddenResponse(response, 'You need to fill all fields correctly.')
		:
		db.query('INSERT INTO location (name) VALUES ($1) RETURNING *', [name])
			.then((result) => {
				createCreatedResponse(response, 'Successfully added location.', result.rows[0]);
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
};

export const updateLocation = (request, response) => {
	const { id, name } = request.body;
	!validateStringTypeLength(name, 1, 40) || !validateNumber(id) ?
		createForbiddenResponse(response, 'You need to fill all fields correctly.')
		:
		db.query('UPDATE location SET name = $2 WHERE id = $1 RETURNING *', [id, name])
			.then((result) => {
				createStatusOKResponse(response, 'Successfully update location.', result.rows[0]);
			}).catch(() => {
				createInternalServerErrorResponse(response);
			});
};

export const deleteLocation = (request, response) => {
	const locationId = parseInt(request.body.id);
	db.query('SELECT * FROM books WHERE location = $1', [locationId])
		.then((result) => {
			result.rows[0] ?
				createForbiddenResponse(response, 'Cannot delete location while books are linked to it.')
				:
				db.query('DELETE FROM location WHERE id = $1 RETURNING *', [locationId])
					.then((result) => {
						createStatusOKResponse(response, 'Successfully deleted location.', result.rows[0]);
					}).catch(() => {
						createInternalServerErrorResponse(response);
					});
		}).catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export default {
	getLocations,
	addLocation,
	updateLocation,
	deleteLocation
};