import { db } from '../../config/db.js';
import { validatePassword, issueJWT } from '../../lib/authUtils.js';
import { createStatusOKResponse, createUnAuthorizedResponse, createInternalServerErrorResponse } from '../responses.js';

export const login = (request, response) => {
	const { name } = request.body;
	const { password } = request.body;

	db.query('SELECT * FROM account WHERE name = $1', [name])
		.then((result) => {
			const account = result.rows[0];
			if (!account) {
				createUnAuthorizedResponse(response, 'You entered wrong username or password.');
			} else {

				const isValidPassword = validatePassword(password, account.hash, account.salt);

				if (isValidPassword) {
					const jwt = issueJWT(account);
					createStatusOKResponse(response, 'Successfully logged in.', { account: { id: account.id, admin: account.admin, name: account.name }, token: jwt.token, expiresIn: jwt.expires });
				} else {
					createUnAuthorizedResponse(response, 'You entered wrong username or password.');
				}
			}
		})
		.catch(() => {
			createInternalServerErrorResponse(response);
		});
};

export default {
	login
};