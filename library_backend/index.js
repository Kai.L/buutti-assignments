import express, { json, urlencoded } from 'express';
import passport from 'passport';
import cors from 'cors';
import passportConfig from './config/passport.js';
import router from './src/routes/router.js';

const app = express();
passportConfig(passport);

app.use(cors());
app.use(passport.initialize());
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(router);
console.log('TESTI!');
const PORT = 3001;

app.listen(PORT, () => {
	console.log(`Server is listening on port: ${PORT}`);
});