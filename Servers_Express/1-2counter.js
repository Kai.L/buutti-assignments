const express = require('express')

const app = express()

let counter = 0

const users = [

]

app.get("/counter/number/:number", (req, res) => {
    counter = +req.params.number
    res.send(`<h1>${counter}</h1>`)
})
app.get("/counter", (req, res) => {
    counter += 1
    res.send(`<h1>${counter}</h1>`)
})
app.get("/counter/name/:name", (req, res) => {
    if (!users[0]) {
        users.push({ name: req.params.name, counter: 1 })
        res.send(`<h1>${req.params.name} was here ${1} times</h1>`)
    } else {
        const userIndex = users.findIndex((u) => u.name === req.params.name)
        if (userIndex === -1) {
            users.push({ name: req.params.name, counter: 1 })
            res.send(`<h1>${req.params.name} was here ${1} times</h1>`)
        } else {
            users[userIndex].counter += 1
            res.send(`<h1>${users[userIndex].name} was here ${users[userIndex].counter} times</h1>`)
        }
    }
})


app.listen(5000)