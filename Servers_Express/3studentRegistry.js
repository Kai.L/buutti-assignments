const express = require('express')
const app = express()
const fs = require("fs")

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

const students = []


app.get('/student/id/:id', (req, res) => {
    const student = students[req.params.id]
    if (student) {
        res.send(`<p> id: ${student.id} Name: ${student.name} Address: ${student.address} Email: ${student.email}</p>`)
    } else {
        res.send(`<p>Student not found!</p>`)
    }
})

app.post('/student', (req, res) => {
    const params = req.query
    const student = {
        name: params.name,
        id: params.id,
        email: params.email,
        address: params.address
    }
    students.push(student)
    fs.writeFile("students.json", JSON.stringify([student]), (err) => {
        if (err) {
            console.log("Could not save student to file");
        }
    });
    res.send(console.log("Student added."))
})
app.put('/student/modify', (req, res) => {
    if (students[req.query.id]) {
        if (req.query.name) {
            students[req.query.id].name = req.query.name
        }
        if (req.query.email) {
            students[req.query.id].email = req.query.email
        }
        if (req.query.address) {
            students[req.query.id].address = req.query.address
        }
        res.send(console.log("Student modified."))
    } else {
        res.send(console.log("Student not found!"))
    }
})
app.delete('/student/remove', (req, res) => {
    if (students[req.query.id]) {
        students.splice(req.query.id)
        res.send(console.log("Student deleted."))
    } else {
        res.send(console.log("Student not found!"))
    }
})
app.listen(5000, () => {
    console.log("Server is listening on port 5000")
})
